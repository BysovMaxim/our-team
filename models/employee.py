from odoo import api, fields, models


class Employee(models.Model):
    _name = "our_team.employee"
    _description = "Employee"

    photo = fields.Binary('Фотография')
    name = fields.Char('Имя')
    surname = fields.Char('Фамилия')
    patronymic = fields.Char('Отчество')
    phone_number = fields.Char('Номер телефона')
    email = fields.Char('Эл. почта')
    unit_id = fields.Many2one('our_team.unit', string="Подразделение")
    
