from odoo import api, fields, models


class Unit(models.Model):
    _name = "our_team.unit"
    _description = "Unit"

    name = fields.Char('Тип')
    employee_ids = fields.One2many('our_team.employee', 'unit_id', string="Сотрудники")