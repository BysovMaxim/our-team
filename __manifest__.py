{
    'name': "Our Team",
    'summary': "Our Team",
    'description': "Our Team module",
    'author': "We",
    'category': "Social Network",
    'version': "0.1",
    'depends': ['base'],
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        #'views/templates.xml',
    ],
}